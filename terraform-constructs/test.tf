
resource "null_resource" "hello_world" {
  count = var.num_instances

  provisioner "local-exec" {
    command = "echo Hello, World! Instance ${count.index + 1}"
  }
}