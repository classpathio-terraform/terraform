provider "aws" {
  profile = "terraform"
  region = "ap-south-1" # Change this to your desired region
}

resource "aws_instance" "pradeep_instance" {
  ami           = "ami-001843b876406202a"  # Change this to the desired AMI ID
  instance_type = "t2.micro"              # Change this to the desired instance type
  key_name = "pradeep-ec2-connect"
  vpc_security_group_ids = [aws_security_group.instance_sg.id]

  tags = {
    Name = "Pradeep-EC2-instance"
  }
}

resource "aws_security_group" "instance_sg" {
  name        = "InstanceSecurityGroup"
  description = "Security group for EC2 instance"
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Be cautious with this setting; restrict to trusted IPs in production
  }
}


output "instance_ips" {
  value = aws_instance.pradeep_instance.public_ip
}
output "instance_public_dns" {
  value = aws_instance.pradeep_instance.public_dns
}
